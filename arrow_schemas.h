#ifndef ARROW_SCHEMAS_H
#define ARROW_SCHEMAS_H

#include <cstdint>
#include <vector>
#include <string>
#include <arrow/api.h>


/*
 * Create Arrow schemas for the data source.
 * For the field builder arguments are
 * name, data type and nullable field.
 */

namespace ugmt {
    std::shared_ptr<arrow::Schema> schema = arrow::schema({
        arrow::field("orbit",   arrow::uint32(),  false),
        arrow::field("bx",      arrow::uint16(),  false),
        arrow::field("interm",  arrow::int8(),    false),
        arrow::field("pt",      arrow::float32(), false),
        arrow::field("ptuncon", arrow::float32(), false),
        arrow::field("charge",  arrow::int8(),    false),
        arrow::field("iso",     arrow::int8(),    false),
        arrow::field("index",   arrow::int8(),    false),
        arrow::field("qual",    arrow::int8(),    false),
        arrow::field("phi",     arrow::float32(), false),
        arrow::field("phie",    arrow::float32(), false),
        arrow::field("eta",     arrow::float32(), false),
        arrow::field("etae",    arrow::float32(), false),
        arrow::field("dxy",     arrow::int8(),    false)
    });

    std::shared_ptr<arrow::Schema> nested_schema = arrow::schema({
        arrow::field("orbit",        arrow::uint32()),
        arrow::field("bx",           arrow::uint16()),
        // muons
        arrow::field("nMuon",       arrow::int8()),
        arrow::field("Muon",        arrow::list(arrow::struct_({
            arrow::field("interm",  arrow::int8()),
            arrow::field("pt",      arrow::float32()),
            arrow::field("ptuncon", arrow::float32()),
            arrow::field("charge",  arrow::int8()),
            arrow::field("iso",     arrow::int8()),
            arrow::field("index",   arrow::int8()),
            arrow::field("qual",    arrow::int8()),
            arrow::field("phi",     arrow::float32()),
            arrow::field("phie",    arrow::float32()),
            arrow::field("eta",     arrow::float32()),
            arrow::field("etae",    arrow::float32()),
            arrow::field("dxy",     arrow::int8()),
        }))),
    });
}



namespace demux {
    std::shared_ptr<arrow::Schema> schema = arrow::schema({
        arrow::field("orbit",     arrow::uint32(),  false),
        arrow::field("bx",        arrow::uint16(),  false),
        arrow::field("ET",        arrow::float32(), false),
        arrow::field("type",      arrow::int8(),    false),
        arrow::field("eta",       arrow::float32(), false),
        arrow::field("phi",       arrow::float32(), false),
        arrow::field("iso",       arrow::int8(),    false),
        arrow::field("ETEt",      arrow::float32(), false),
        arrow::field("HTEt",      arrow::float32(), false),
        arrow::field("ETmissEt",  arrow::float32(), false),
        arrow::field("HTmissEt",  arrow::float32(), false),
        arrow::field("ETmissPhi", arrow::float32(), false),
        arrow::field("HTmissPhi", arrow::float32(), false)
    });

    std::shared_ptr<arrow::Schema> nested_schema = arrow::schema({
        arrow::field("orbit",      arrow::uint32()),
        arrow::field("bx",         arrow::uint16()),
        // jets
        arrow::field("nJet",       arrow::int8()),
        arrow::field("Jet",        arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("eta",    arrow::float32()),
            arrow::field("phi",    arrow::float32()),
            arrow::field("iso",    arrow::int8()),
        }))),
        // egammas
        arrow::field("nEGamma",    arrow::int8()),
        arrow::field("EGamma",     arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("eta",    arrow::float32()),
            arrow::field("phi",    arrow::float32()),
            arrow::field("iso",    arrow::int8()),
        }))),
        // taus
        arrow::field("nTau",       arrow::int8()),
        arrow::field("Tau",        arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("eta",    arrow::float32()),
            arrow::field("phi",    arrow::float32()),
            arrow::field("iso",    arrow::int8()),
        }))),
        // sums
        arrow::field("SumET",      arrow::struct_({
            arrow::field("ET",     arrow::float32()),
        })),
        arrow::field("SumHT",      arrow::struct_({
            arrow::field("ET",     arrow::float32()),
        })),
        arrow::field("SumMET",     arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("phi",    arrow::float32()),
        })),
        arrow::field("SumMHT",     arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("phi",    arrow::float32()),
        }))
    });
}



namespace ugt {
    using arrow::boolean;
    using arrow::field;
    std::shared_ptr<arrow::Schema> schema = arrow::schema({
        arrow::field("orbits_seen",    arrow::uint32()),
        arrow::field("orbits_dropped", arrow::uint32()),
        arrow::field("bx",             arrow::uint16()),
        // 000 to 099
        field("algo000", boolean()), field("algo001", boolean()), field("algo002", boolean()), field("algo003", boolean()), field("algo004", boolean()), field("algo005", boolean()), field("algo006", boolean()), field("algo007", boolean()), field("algo008", boolean()), field("algo009", boolean()),
        field("algo010", boolean()), field("algo011", boolean()), field("algo012", boolean()), field("algo013", boolean()), field("algo014", boolean()), field("algo015", boolean()), field("algo016", boolean()), field("algo017", boolean()), field("algo018", boolean()), field("algo019", boolean()),
        field("algo020", boolean()), field("algo021", boolean()), field("algo022", boolean()), field("algo023", boolean()), field("algo024", boolean()), field("algo025", boolean()), field("algo026", boolean()), field("algo027", boolean()), field("algo028", boolean()), field("algo029", boolean()),
        field("algo030", boolean()), field("algo031", boolean()), field("algo032", boolean()), field("algo033", boolean()), field("algo034", boolean()), field("algo035", boolean()), field("algo036", boolean()), field("algo037", boolean()), field("algo038", boolean()), field("algo039", boolean()),
        field("algo040", boolean()), field("algo041", boolean()), field("algo042", boolean()), field("algo043", boolean()), field("algo044", boolean()), field("algo045", boolean()), field("algo046", boolean()), field("algo047", boolean()), field("algo048", boolean()), field("algo049", boolean()),
        field("algo050", boolean()), field("algo051", boolean()), field("algo052", boolean()), field("algo053", boolean()), field("algo054", boolean()), field("algo055", boolean()), field("algo056", boolean()), field("algo057", boolean()), field("algo058", boolean()), field("algo059", boolean()),
        field("algo060", boolean()), field("algo061", boolean()), field("algo062", boolean()), field("algo063", boolean()), field("algo064", boolean()), field("algo065", boolean()), field("algo066", boolean()), field("algo067", boolean()), field("algo068", boolean()), field("algo069", boolean()),
        field("algo070", boolean()), field("algo071", boolean()), field("algo072", boolean()), field("algo073", boolean()), field("algo074", boolean()), field("algo075", boolean()), field("algo076", boolean()), field("algo077", boolean()), field("algo078", boolean()), field("algo079", boolean()),
        field("algo080", boolean()), field("algo081", boolean()), field("algo082", boolean()), field("algo083", boolean()), field("algo084", boolean()), field("algo085", boolean()), field("algo086", boolean()), field("algo087", boolean()), field("algo088", boolean()), field("algo089", boolean()),
        field("algo090", boolean()), field("algo091", boolean()), field("algo092", boolean()), field("algo093", boolean()), field("algo094", boolean()), field("algo095", boolean()), field("algo096", boolean()), field("algo097", boolean()), field("algo098", boolean()), field("algo099", boolean()),
        // 100 to 199
        field("algo100", boolean()), field("algo101", boolean()), field("algo102", boolean()), field("algo103", boolean()), field("algo104", boolean()), field("algo105", boolean()), field("algo106", boolean()), field("algo107", boolean()), field("algo108", boolean()), field("algo109", boolean()),
        field("algo110", boolean()), field("algo111", boolean()), field("algo112", boolean()), field("algo113", boolean()), field("algo114", boolean()), field("algo115", boolean()), field("algo116", boolean()), field("algo117", boolean()), field("algo118", boolean()), field("algo119", boolean()),
        field("algo120", boolean()), field("algo121", boolean()), field("algo122", boolean()), field("algo123", boolean()), field("algo124", boolean()), field("algo125", boolean()), field("algo126", boolean()), field("algo127", boolean()), field("algo128", boolean()), field("algo129", boolean()),
        field("algo130", boolean()), field("algo131", boolean()), field("algo132", boolean()), field("algo133", boolean()), field("algo134", boolean()), field("algo135", boolean()), field("algo136", boolean()), field("algo137", boolean()), field("algo138", boolean()), field("algo139", boolean()),
        field("algo140", boolean()), field("algo141", boolean()), field("algo142", boolean()), field("algo143", boolean()), field("algo144", boolean()), field("algo145", boolean()), field("algo146", boolean()), field("algo147", boolean()), field("algo148", boolean()), field("algo149", boolean()),
        field("algo150", boolean()), field("algo151", boolean()), field("algo152", boolean()), field("algo153", boolean()), field("algo154", boolean()), field("algo155", boolean()), field("algo156", boolean()), field("algo157", boolean()), field("algo158", boolean()), field("algo159", boolean()),
        field("algo160", boolean()), field("algo161", boolean()), field("algo162", boolean()), field("algo163", boolean()), field("algo164", boolean()), field("algo165", boolean()), field("algo166", boolean()), field("algo167", boolean()), field("algo168", boolean()), field("algo169", boolean()),
        field("algo170", boolean()), field("algo171", boolean()), field("algo172", boolean()), field("algo173", boolean()), field("algo174", boolean()), field("algo175", boolean()), field("algo176", boolean()), field("algo177", boolean()), field("algo178", boolean()), field("algo179", boolean()),
        field("algo180", boolean()), field("algo181", boolean()), field("algo182", boolean()), field("algo183", boolean()), field("algo184", boolean()), field("algo185", boolean()), field("algo186", boolean()), field("algo187", boolean()), field("algo188", boolean()), field("algo189", boolean()),
        field("algo190", boolean()), field("algo191", boolean()), field("algo192", boolean()), field("algo193", boolean()), field("algo194", boolean()), field("algo195", boolean()), field("algo196", boolean()), field("algo197", boolean()), field("algo198", boolean()), field("algo199", boolean()),
        // 200 to 299
        field("algo200", boolean()), field("algo201", boolean()), field("algo202", boolean()), field("algo203", boolean()), field("algo204", boolean()), field("algo205", boolean()), field("algo206", boolean()), field("algo207", boolean()), field("algo208", boolean()), field("algo209", boolean()),
        field("algo210", boolean()), field("algo211", boolean()), field("algo212", boolean()), field("algo213", boolean()), field("algo214", boolean()), field("algo215", boolean()), field("algo216", boolean()), field("algo217", boolean()), field("algo218", boolean()), field("algo219", boolean()),
        field("algo220", boolean()), field("algo221", boolean()), field("algo222", boolean()), field("algo223", boolean()), field("algo224", boolean()), field("algo225", boolean()), field("algo226", boolean()), field("algo227", boolean()), field("algo228", boolean()), field("algo229", boolean()),
        field("algo230", boolean()), field("algo231", boolean()), field("algo232", boolean()), field("algo233", boolean()), field("algo234", boolean()), field("algo235", boolean()), field("algo236", boolean()), field("algo237", boolean()), field("algo238", boolean()), field("algo239", boolean()),
        field("algo240", boolean()), field("algo241", boolean()), field("algo242", boolean()), field("algo243", boolean()), field("algo244", boolean()), field("algo245", boolean()), field("algo246", boolean()), field("algo247", boolean()), field("algo248", boolean()), field("algo249", boolean()),
        field("algo250", boolean()), field("algo251", boolean()), field("algo252", boolean()), field("algo253", boolean()), field("algo254", boolean()), field("algo255", boolean()), field("algo256", boolean()), field("algo257", boolean()), field("algo258", boolean()), field("algo259", boolean()),
        field("algo260", boolean()), field("algo261", boolean()), field("algo262", boolean()), field("algo263", boolean()), field("algo264", boolean()), field("algo265", boolean()), field("algo266", boolean()), field("algo267", boolean()), field("algo268", boolean()), field("algo269", boolean()),
        field("algo270", boolean()), field("algo271", boolean()), field("algo272", boolean()), field("algo273", boolean()), field("algo274", boolean()), field("algo275", boolean()), field("algo276", boolean()), field("algo277", boolean()), field("algo278", boolean()), field("algo279", boolean()),
        field("algo280", boolean()), field("algo281", boolean()), field("algo282", boolean()), field("algo283", boolean()), field("algo284", boolean()), field("algo285", boolean()), field("algo286", boolean()), field("algo287", boolean()), field("algo288", boolean()), field("algo289", boolean()),
        field("algo290", boolean()), field("algo291", boolean()), field("algo292", boolean()), field("algo293", boolean()), field("algo294", boolean()), field("algo295", boolean()), field("algo296", boolean()), field("algo297", boolean()), field("algo298", boolean()), field("algo299", boolean()),
        // 300 to 399
        field("algo300", boolean()), field("algo301", boolean()), field("algo302", boolean()), field("algo303", boolean()), field("algo304", boolean()), field("algo305", boolean()), field("algo306", boolean()), field("algo307", boolean()), field("algo308", boolean()), field("algo309", boolean()),
        field("algo310", boolean()), field("algo311", boolean()), field("algo312", boolean()), field("algo313", boolean()), field("algo314", boolean()), field("algo315", boolean()), field("algo316", boolean()), field("algo317", boolean()), field("algo318", boolean()), field("algo319", boolean()),
        field("algo320", boolean()), field("algo321", boolean()), field("algo322", boolean()), field("algo323", boolean()), field("algo324", boolean()), field("algo325", boolean()), field("algo326", boolean()), field("algo327", boolean()), field("algo328", boolean()), field("algo329", boolean()),
        field("algo330", boolean()), field("algo331", boolean()), field("algo332", boolean()), field("algo333", boolean()), field("algo334", boolean()), field("algo335", boolean()), field("algo336", boolean()), field("algo337", boolean()), field("algo338", boolean()), field("algo339", boolean()),
        field("algo340", boolean()), field("algo341", boolean()), field("algo342", boolean()), field("algo343", boolean()), field("algo344", boolean()), field("algo345", boolean()), field("algo346", boolean()), field("algo347", boolean()), field("algo348", boolean()), field("algo349", boolean()),
        field("algo350", boolean()), field("algo351", boolean()), field("algo352", boolean()), field("algo353", boolean()), field("algo354", boolean()), field("algo355", boolean()), field("algo356", boolean()), field("algo357", boolean()), field("algo358", boolean()), field("algo359", boolean()),
        field("algo360", boolean()), field("algo361", boolean()), field("algo362", boolean()), field("algo363", boolean()), field("algo364", boolean()), field("algo365", boolean()), field("algo366", boolean()), field("algo367", boolean()), field("algo368", boolean()), field("algo369", boolean()),
        field("algo370", boolean()), field("algo371", boolean()), field("algo372", boolean()), field("algo373", boolean()), field("algo374", boolean()), field("algo375", boolean()), field("algo376", boolean()), field("algo377", boolean()), field("algo378", boolean()), field("algo379", boolean()),
        field("algo380", boolean()), field("algo381", boolean()), field("algo382", boolean()), field("algo383", boolean()), field("algo384", boolean()), field("algo385", boolean()), field("algo386", boolean()), field("algo387", boolean()), field("algo388", boolean()), field("algo389", boolean()),
        field("algo390", boolean()), field("algo391", boolean()), field("algo392", boolean()), field("algo393", boolean()), field("algo394", boolean()), field("algo395", boolean()), field("algo396", boolean()), field("algo397", boolean()), field("algo398", boolean()), field("algo399", boolean()),
        // 400 to 499
        field("algo400", boolean()), field("algo401", boolean()), field("algo402", boolean()), field("algo403", boolean()), field("algo404", boolean()), field("algo405", boolean()), field("algo406", boolean()), field("algo407", boolean()), field("algo408", boolean()), field("algo409", boolean()),
        field("algo410", boolean()), field("algo411", boolean()), field("algo412", boolean()), field("algo413", boolean()), field("algo414", boolean()), field("algo415", boolean()), field("algo416", boolean()), field("algo417", boolean()), field("algo418", boolean()), field("algo419", boolean()),
        field("algo420", boolean()), field("algo421", boolean()), field("algo422", boolean()), field("algo423", boolean()), field("algo424", boolean()), field("algo425", boolean()), field("algo426", boolean()), field("algo427", boolean()), field("algo428", boolean()), field("algo429", boolean()),
        field("algo430", boolean()), field("algo431", boolean()), field("algo432", boolean()), field("algo433", boolean()), field("algo434", boolean()), field("algo435", boolean()), field("algo436", boolean()), field("algo437", boolean()), field("algo438", boolean()), field("algo439", boolean()),
        field("algo440", boolean()), field("algo441", boolean()), field("algo442", boolean()), field("algo443", boolean()), field("algo444", boolean()), field("algo445", boolean()), field("algo446", boolean()), field("algo447", boolean()), field("algo448", boolean()), field("algo449", boolean()),
        field("algo450", boolean()), field("algo451", boolean()), field("algo452", boolean()), field("algo453", boolean()), field("algo454", boolean()), field("algo455", boolean()), field("algo456", boolean()), field("algo457", boolean()), field("algo458", boolean()), field("algo459", boolean()),
        field("algo460", boolean()), field("algo461", boolean()), field("algo462", boolean()), field("algo463", boolean()), field("algo464", boolean()), field("algo465", boolean()), field("algo466", boolean()), field("algo467", boolean()), field("algo468", boolean()), field("algo469", boolean()),
        field("algo470", boolean()), field("algo471", boolean()), field("algo472", boolean()), field("algo473", boolean()), field("algo474", boolean()), field("algo475", boolean()), field("algo476", boolean()), field("algo477", boolean()), field("algo478", boolean()), field("algo479", boolean()),
        field("algo480", boolean()), field("algo481", boolean()), field("algo482", boolean()), field("algo483", boolean()), field("algo484", boolean()), field("algo485", boolean()), field("algo486", boolean()), field("algo487", boolean()), field("algo488", boolean()), field("algo489", boolean()),
        field("algo490", boolean()), field("algo491", boolean()), field("algo492", boolean()), field("algo493", boolean()), field("algo494", boolean()), field("algo495", boolean()), field("algo496", boolean()), field("algo497", boolean()), field("algo498", boolean()), field("algo499", boolean()),
        // 500 to 511
        field("algo500", boolean()), field("algo501", boolean()), field("algo502", boolean()), field("algo503", boolean()), field("algo504", boolean()), field("algo505", boolean()), field("algo506", boolean()), field("algo507", boolean()), field("algo508", boolean()), field("algo509", boolean()),
        field("algo510", boolean()), field("algo511", boolean())
    });
}



namespace bmtf {
    std::shared_ptr<arrow::Schema> schema = arrow::schema({
        arrow::field("orbit",          arrow::uint32()),
        arrow::field("orbits_dropped", arrow::uint32()),
        arrow::field("bx",             arrow::uint16()),
        arrow::field("valid",          arrow::int16()),
        arrow::field("phi",            arrow::int16()),
        arrow::field("phiB",           arrow::int16()),
        arrow::field("qual",           arrow::int16()),
        arrow::field("eta",            arrow::int16()),
        arrow::field("qeta",           arrow::int16()),
        arrow::field("station",        arrow::int16()),
        arrow::field("wheel",          arrow::int16()),
        arrow::field("reserved",       arrow::int16()),
        arrow::field("lid",            arrow::int16())
    });
}

// const std::vector<std::string> ugmt::arrow_data_block::columns  = {"orbit", "bx", "interm", "pt", "ptunconstrained", "charge", "iso", "index", "qual", "phi", "phie", "eta", "etae", "dxy"};
// const std::vector<std::string> demux::arrow_data_block::columns = {"orbit", "bx", "ET", "type", "eta", "phi", "iso", "ETEt", "HTEt", "ETmissEt", "HTmissEt", "ETmissPhi", "HTmissPhi"};



namespace multisrc {
    std::shared_ptr<arrow::Schema> nested_schema = arrow::schema({
        arrow::field("orbit",        arrow::uint32()),
        arrow::field("bx",           arrow::uint16()),
        // muons
        arrow::field("nMuon",       arrow::int8()),
        arrow::field("Muon",        arrow::list(arrow::struct_({
            arrow::field("interm",  arrow::int8()),
            arrow::field("pt",      arrow::float32()),
            arrow::field("ptuncon", arrow::float32()),
            arrow::field("charge",  arrow::int8()),
            arrow::field("iso",     arrow::int8()),
            arrow::field("index",   arrow::int8()),
            arrow::field("qual",    arrow::int8()),
            arrow::field("phi",     arrow::float32()),
            arrow::field("phie",    arrow::float32()),
            arrow::field("eta",     arrow::float32()),
            arrow::field("etae",    arrow::float32()),
            arrow::field("dxy",     arrow::int8()),
        }))),
        // jets
        arrow::field("nJet",       arrow::int8()),
        arrow::field("Jet",        arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("eta",    arrow::float32()),
            arrow::field("phi",    arrow::float32()),
            arrow::field("iso",    arrow::int8()),
        }))),
        // egammas
        arrow::field("nEGamma",    arrow::int8()),
        arrow::field("EGamma",     arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("eta",    arrow::float32()),
            arrow::field("phi",    arrow::float32()),
            arrow::field("iso",    arrow::int8()),
        }))),
        // taus
        arrow::field("nTau",       arrow::int8()),
        arrow::field("Tau",        arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("eta",    arrow::float32()),
            arrow::field("phi",    arrow::float32()),
            arrow::field("iso",    arrow::int8()),
        }))),
        // sums
        arrow::field("SumET",      arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
        }))),
        arrow::field("SumHT",      arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
        }))),
        arrow::field("SumMET",     arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("phi",    arrow::float32()),
        }))),
        arrow::field("SumMHT",     arrow::list(arrow::struct_({
            arrow::field("ET",     arrow::float32()),
            arrow::field("phi",    arrow::float32()),
        })))
    });
}

#endif
