#ifndef ARROW_BLOCKS_H
#define ARROW_BLOCKS_H

#include <cstdint>
#include <vector>
#include <string>
#include <arrow/api.h>
#include <parquet/arrow/writer.h>
#include "scales.h"



namespace ugmt {
    struct arrow_data_block {
        // block n columns and column names
        static const unsigned int ncols = 14;
        static const std::vector<std::string> columns;

        // arrow builders
        arrow::UInt32Builder builderOrbit;
        arrow::UInt16Builder builderBx;
        arrow::Int8Builder   builderInterm;
        arrow::FloatBuilder  builderPt;
        arrow::FloatBuilder  builderPtuncon;
        arrow::Int8Builder   builderCharge;
        arrow::Int8Builder   builderIso;
        arrow::Int8Builder   builderIndex;
        arrow::Int8Builder   builderQual;
        arrow::FloatBuilder  builderPhi;
        arrow::FloatBuilder  builderPhie;
        arrow::FloatBuilder  builderEta;
        arrow::FloatBuilder  builderEtae;
        arrow::Int8Builder   builderDxy;

        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        inline void Append(uint32_t orbit, uint32_t bx, uint32_t interm,
                    float fpt, float fptuncon,
                    int32_t chrg, uint32_t iso, uint32_t index, uint32_t qual,
                    float fphi, float fphiext, float feta, float fetaext,
                    int32_t idxy) {
            PARQUET_THROW_NOT_OK(builderOrbit.Append(orbit));
            PARQUET_THROW_NOT_OK(builderBx.Append(bx));
            PARQUET_THROW_NOT_OK(builderInterm.Append(interm));
            PARQUET_THROW_NOT_OK(builderPt.Append(fpt));
            PARQUET_THROW_NOT_OK(builderPtuncon.Append(fptuncon));
            PARQUET_THROW_NOT_OK(builderCharge.Append(chrg));
            PARQUET_THROW_NOT_OK(builderIso.Append(iso));
            PARQUET_THROW_NOT_OK(builderIndex.Append(index));
            PARQUET_THROW_NOT_OK(builderQual.Append(qual));
            PARQUET_THROW_NOT_OK(builderPhi.Append(fphi));
            PARQUET_THROW_NOT_OK(builderPhie.Append(fphiext));
            PARQUET_THROW_NOT_OK(builderEta.Append(feta));
            PARQUET_THROW_NOT_OK(builderEtae.Append(fetaext));
            PARQUET_THROW_NOT_OK(builderDxy.Append(idxy));
        };

        void Finish() {
            PARQUET_THROW_NOT_OK(builderOrbit.Finish(&arrayData[0]));
            PARQUET_THROW_NOT_OK(builderBx.Finish(&arrayData[1]));
            PARQUET_THROW_NOT_OK(builderInterm.Finish(&arrayData[2]));
            PARQUET_THROW_NOT_OK(builderPt.Finish(&arrayData[3]));
            PARQUET_THROW_NOT_OK(builderPtuncon.Finish(&arrayData[4]));
            PARQUET_THROW_NOT_OK(builderCharge.Finish(&arrayData[5]));
            PARQUET_THROW_NOT_OK(builderIso.Finish(&arrayData[6]));
            PARQUET_THROW_NOT_OK(builderIndex.Finish(&arrayData[7]));
            PARQUET_THROW_NOT_OK(builderQual.Finish(&arrayData[8]));
            PARQUET_THROW_NOT_OK(builderPhi.Finish(&arrayData[9]));
            PARQUET_THROW_NOT_OK(builderPhie.Finish(&arrayData[10]));
            PARQUET_THROW_NOT_OK(builderEta.Finish(&arrayData[11]));
            PARQUET_THROW_NOT_OK(builderEtae.Finish(&arrayData[12]));
            PARQUET_THROW_NOT_OK(builderDxy.Finish(&arrayData[13]));
        };
    };


    struct arrow_data_block_nested {
        // block n columns and column names
        // static const unsigned int ncols = 15;
        static const unsigned int ncols = 4;
        static const std::vector<std::string> columns;

        // arrow memory pool
        arrow::MemoryPool* pool;

        // arrow builders
        std::shared_ptr<arrow::UInt32Builder> builder_orbit;
        std::shared_ptr<arrow::UInt16Builder> builder_bx;

        std::shared_ptr<arrow::Int8Builder>   builder_nMuon;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_interm;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_pt;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_ptuncon;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_charge;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_iso;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_index;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_qual;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_phi;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_phie;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_etae;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_dxy;
        std::shared_ptr<arrow::StructBuilder> builder_Muon;

        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_Muon_pars;

        std::shared_ptr<arrow::ListBuilder>   listBuilder_Muon;


        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        // struct constructor
        arrow_data_block_nested() {
            pool = arrow::default_memory_pool();

            std::shared_ptr<arrow::DataType> muon_type = arrow::struct_({
                arrow::field("Muon_interm",  arrow::int8()),
                arrow::field("Muon_pt",      arrow::float32()),
                arrow::field("Muon_ptuncon", arrow::float32()),
                arrow::field("Muon_charge",  arrow::int8()),
                arrow::field("Muon_iso",     arrow::int8()),
                arrow::field("Muon_index",   arrow::int8()),
                arrow::field("Muon_qual",    arrow::int8()),
                arrow::field("Muon_phi",     arrow::float32()),
                arrow::field("Muon_phie",    arrow::float32()),
                arrow::field("Muon_eta",     arrow::float32()),
                arrow::field("Muon_etae",    arrow::float32()),
                arrow::field("Muon_dxy",     arrow::int8()),
            });

            builder_orbit = std::make_shared<arrow::UInt32Builder>(pool);
            builder_bx    = std::make_shared<arrow::UInt16Builder>(pool);
            builder_nMuon = std::make_shared<arrow::Int8Builder>(pool);

            builder_Muon_interm  = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_pt      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_ptuncon = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_charge  = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_iso     = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_index   = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_qual    = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_phi     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_phie    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_eta     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_etae    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_dxy     = std::make_shared<arrow::Int8Builder>(pool);

            vBuilder_Muon_pars   = {builder_Muon_interm,
                                    builder_Muon_pt,
                                    builder_Muon_ptuncon,
                                    builder_Muon_charge,
                                    builder_Muon_iso,
                                    builder_Muon_index,
                                    builder_Muon_qual,
                                    builder_Muon_phi,
                                    builder_Muon_phie,
                                    builder_Muon_eta,
                                    builder_Muon_etae,
                                    builder_Muon_dxy};

            builder_Muon = std::make_shared<arrow::StructBuilder>(muon_type, pool, std::move(vBuilder_Muon_pars));

            listBuilder_Muon = std::make_shared<arrow::ListBuilder>(pool, builder_Muon);
        }

        // append bx data (and initialize lists)
        inline void BxAppend(uint32_t orbit,
                             uint32_t bx) {
            PARQUET_THROW_NOT_OK(builder_orbit->Append(orbit));
            PARQUET_THROW_NOT_OK(builder_bx->Append(bx));

            PARQUET_THROW_NOT_OK(listBuilder_Muon->Append());
        }

        // append number of muons data
        inline void NMuonAppend(uint32_t nMuon) {
            PARQUET_THROW_NOT_OK(builder_nMuon->Append(nMuon));
        }

        // append object data into list
        inline void MuonAppend(uint32_t interm,
                               float fpt, float fptuncon,
                               int32_t chrg,
                               uint32_t iso, uint32_t index, uint32_t qual,
                               float fphi, float fphiext, float feta, float fetaext,
                               int32_t idxy) {
            PARQUET_THROW_NOT_OK(builder_Muon->Append());

            PARQUET_THROW_NOT_OK(builder_Muon_interm->Append(interm));
            PARQUET_THROW_NOT_OK(builder_Muon_pt->Append(fpt));
            PARQUET_THROW_NOT_OK(builder_Muon_ptuncon->Append(fptuncon));
            PARQUET_THROW_NOT_OK(builder_Muon_charge->Append(chrg));
            PARQUET_THROW_NOT_OK(builder_Muon_iso->Append(iso));
            PARQUET_THROW_NOT_OK(builder_Muon_index->Append(index));
            PARQUET_THROW_NOT_OK(builder_Muon_qual->Append(qual));
            PARQUET_THROW_NOT_OK(builder_Muon_phi->Append(fphi));
            PARQUET_THROW_NOT_OK(builder_Muon_phie->Append(fphiext));
            PARQUET_THROW_NOT_OK(builder_Muon_eta->Append(feta));
            PARQUET_THROW_NOT_OK(builder_Muon_etae->Append(fetaext));
            PARQUET_THROW_NOT_OK(builder_Muon_dxy->Append(idxy));
        };

        // close builders and arrays
        void Finish() {
            PARQUET_THROW_NOT_OK(builder_orbit->Finish(&arrayData[0]));
            PARQUET_THROW_NOT_OK(builder_bx->Finish(&arrayData[1]));
            PARQUET_THROW_NOT_OK(builder_nMuon->Finish(&arrayData[2]));
            PARQUET_THROW_NOT_OK(listBuilder_Muon->Finish(&arrayData[3]));
        };
    };
}



namespace demux {
    struct arrow_data_block {
        // block n columns and column names
        static const unsigned int ncols = 13;
        static const std::vector<std::string> columns;

        // arrow builders
        arrow::UInt32Builder builderOrbit;
        arrow::UInt16Builder builderBx;
        arrow::FloatBuilder  builderET;
        arrow::Int8Builder   builderType;
        arrow::FloatBuilder  builderEta;
        arrow::FloatBuilder  builderPhi;
        arrow::Int8Builder   builderIso;
        arrow::FloatBuilder  builderETEt;
        arrow::FloatBuilder  builderHTEt;
        arrow::FloatBuilder  builderETmissEt;
        arrow::FloatBuilder  builderHTmissEt;
        arrow::FloatBuilder  builderETmissPhi;
        arrow::FloatBuilder  builderHTmissPhi;

        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        inline void Append(uint32_t orbit, uint32_t bx,
                           float fET,
                           int32_t type,
                           float feta, float fphi,
                           int32_t iso,
                           float fETEt, float fHTEt, float fETmissEt, float fHTmissEt, float fETmissPhi, float fHTmissPhi) {
            PARQUET_THROW_NOT_OK(builderOrbit.Append(orbit));
            PARQUET_THROW_NOT_OK(builderBx.Append(bx));
            PARQUET_THROW_NOT_OK(builderET.Append(fET));
            PARQUET_THROW_NOT_OK(builderType.Append(type));
            PARQUET_THROW_NOT_OK(builderEta.Append(feta));
            PARQUET_THROW_NOT_OK(builderPhi.Append(fphi));
            PARQUET_THROW_NOT_OK(builderIso.Append(iso));
            PARQUET_THROW_NOT_OK(builderETEt.Append(fETEt));
            PARQUET_THROW_NOT_OK(builderHTEt.Append(fHTEt));
            PARQUET_THROW_NOT_OK(builderETmissEt.Append(fETmissEt));
            PARQUET_THROW_NOT_OK(builderHTmissEt.Append(fHTmissEt));
            PARQUET_THROW_NOT_OK(builderETmissPhi.Append(fETmissPhi));
            PARQUET_THROW_NOT_OK(builderHTmissPhi.Append(fHTmissPhi));
        }

        void Finish() {
            PARQUET_THROW_NOT_OK(builderOrbit.Finish(&arrayData[0]));
            PARQUET_THROW_NOT_OK(builderBx.Finish(&arrayData[1]));
            PARQUET_THROW_NOT_OK(builderET.Finish(&arrayData[2]));
            PARQUET_THROW_NOT_OK(builderType.Finish(&arrayData[3]));
            PARQUET_THROW_NOT_OK(builderEta.Finish(&arrayData[4]));
            PARQUET_THROW_NOT_OK(builderPhi.Finish(&arrayData[5]));
            PARQUET_THROW_NOT_OK(builderIso.Finish(&arrayData[6]));
            PARQUET_THROW_NOT_OK(builderETEt.Finish(&arrayData[7]));
            PARQUET_THROW_NOT_OK(builderHTEt.Finish(&arrayData[8]));
            PARQUET_THROW_NOT_OK(builderETmissEt.Finish(&arrayData[9]));
            PARQUET_THROW_NOT_OK(builderHTmissEt.Finish(&arrayData[10]));
            PARQUET_THROW_NOT_OK(builderETmissPhi.Finish(&arrayData[11]));
            PARQUET_THROW_NOT_OK(builderHTmissPhi.Finish(&arrayData[12]));
        };
    };


    struct arrow_data_block_nested {
        // block n columns and column names
        static const unsigned int ncols = 12;
        static const std::vector<std::string> columns;

        // arrow memory pool
        arrow::MemoryPool* pool;

        // arrow builders
        std::shared_ptr<arrow::UInt32Builder> builder_orbit;
        std::shared_ptr<arrow::UInt16Builder> builder_bx;

        std::shared_ptr<arrow::Int8Builder>   builder_nJet;
        std::shared_ptr<arrow::FloatBuilder>  builder_Jet_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_Jet_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_Jet_phi;
        std::shared_ptr<arrow::Int8Builder>   builder_Jet_iso;

        std::shared_ptr<arrow::Int8Builder>   builder_nEGamma;
        std::shared_ptr<arrow::FloatBuilder>  builder_EGamma_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_EGamma_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_EGamma_phi;
        std::shared_ptr<arrow::Int8Builder>   builder_EGamma_iso;

        std::shared_ptr<arrow::Int8Builder>   builder_nTau;
        std::shared_ptr<arrow::FloatBuilder>  builder_Tau_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_Tau_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_Tau_phi;
        std::shared_ptr<arrow::Int8Builder>   builder_Tau_iso;

        std::shared_ptr<arrow::FloatBuilder>  builder_SumET_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumHT_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMET_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMHT_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMET_phi;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMHT_phi;

        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_Jet_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_EGamma_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_Tau_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumET_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumHT_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumMET_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumMHT_pars;

        std::shared_ptr<arrow::StructBuilder> builder_Jet;
        std::shared_ptr<arrow::StructBuilder> builder_EGamma;
        std::shared_ptr<arrow::StructBuilder> builder_Tau;
        std::shared_ptr<arrow::StructBuilder> builder_SumET;
        std::shared_ptr<arrow::StructBuilder> builder_SumHT;
        std::shared_ptr<arrow::StructBuilder> builder_SumMET;
        std::shared_ptr<arrow::StructBuilder> builder_SumMHT;

        std::shared_ptr<arrow::ListBuilder>   listBuilder_Jet;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_EGamma;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_Tau;
        // std::shared_ptr<arrow::ListBuilder>   listBuilder_SumET;
        // std::shared_ptr<arrow::ListBuilder>   listBuilder_SumHT;
        // std::shared_ptr<arrow::ListBuilder>   listBuilder_SumMET;
        // std::shared_ptr<arrow::ListBuilder>   listBuilder_SumMHT;

        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        // struct constructor
        arrow_data_block_nested() {
            pool = arrow::default_memory_pool();

            std::shared_ptr<arrow::DataType> Jet_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("eta",    arrow::float32()),
                arrow::field("phi",    arrow::float32()),
                arrow::field("iso",    arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> EGamma_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("eta",    arrow::float32()),
                arrow::field("phi",    arrow::float32()),
                arrow::field("iso",    arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> Tau_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("eta",    arrow::float32()),
                arrow::field("phi",    arrow::float32()),
                arrow::field("iso",    arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> SumET_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
            });

            std::shared_ptr<arrow::DataType> SumHT_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
            });

            std::shared_ptr<arrow::DataType> SumMET_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("phi",    arrow::float32()),
            });

            std::shared_ptr<arrow::DataType> SumMHT_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("phi",    arrow::float32()),
            });

            builder_orbit       = std::make_shared<arrow::UInt32Builder>(pool);
            builder_bx          = std::make_shared<arrow::UInt16Builder>(pool);

            builder_nJet        = std::make_shared<arrow::Int8Builder>(pool);
            builder_Jet_ET      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Jet_eta     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Jet_phi     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Jet_iso     = std::make_shared<arrow::Int8Builder>(pool);

            builder_nEGamma     = std::make_shared<arrow::Int8Builder>(pool);
            builder_EGamma_ET   = std::make_shared<arrow::FloatBuilder>(pool);
            builder_EGamma_eta  = std::make_shared<arrow::FloatBuilder>(pool);
            builder_EGamma_phi  = std::make_shared<arrow::FloatBuilder>(pool);
            builder_EGamma_iso  = std::make_shared<arrow::Int8Builder>(pool);

            builder_nTau        = std::make_shared<arrow::Int8Builder>(pool);
            builder_Tau_ET      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Tau_eta     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Tau_phi     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Tau_iso     = std::make_shared<arrow::Int8Builder>(pool);

            builder_SumET_ET    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumHT_ET    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMET_ET   = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMHT_ET   = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMET_phi  = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMHT_phi  = std::make_shared<arrow::FloatBuilder>(pool);

            builder_orbit     ->Reserve(1048576*128);
            builder_bx        ->Reserve(1048576*128);
            builder_nJet      ->Reserve(1048576*128);
            builder_Jet_ET    ->Reserve(1048576*128);
            builder_Jet_eta   ->Reserve(1048576*128);
            builder_Jet_phi   ->Reserve(1048576*128);
            builder_Jet_iso   ->Reserve(1048576*128);
            builder_nEGamma   ->Reserve(1048576*128);
            builder_EGamma_ET ->Reserve(1048576*128);
            builder_EGamma_eta->Reserve(1048576*128);
            builder_EGamma_phi->Reserve(1048576*128);
            builder_EGamma_iso->Reserve(1048576*128);
            builder_nTau      ->Reserve(1048576*128);
            builder_Tau_ET    ->Reserve(1048576*128);
            builder_Tau_eta   ->Reserve(1048576*128);
            builder_Tau_phi   ->Reserve(1048576*128);
            builder_Tau_iso   ->Reserve(1048576*128);
            builder_SumET_ET  ->Reserve(1048576*128);
            builder_SumHT_ET  ->Reserve(1048576*128);
            builder_SumMET_ET ->Reserve(1048576*128);
            builder_SumMHT_ET ->Reserve(1048576*128);
            builder_SumMET_phi->Reserve(1048576*128);
            builder_SumMHT_phi->Reserve(1048576*128);

            vBuilder_Jet_pars    = {builder_Jet_ET, builder_Jet_eta, builder_Jet_phi, builder_Jet_iso};
            vBuilder_EGamma_pars = {builder_EGamma_ET, builder_EGamma_eta, builder_EGamma_phi, builder_EGamma_iso};
            vBuilder_Tau_pars    = {builder_Tau_ET, builder_Tau_eta, builder_Tau_phi, builder_Tau_iso};
            vBuilder_SumET_pars  = {builder_SumET_ET};
            vBuilder_SumHT_pars  = {builder_SumHT_ET};
            vBuilder_SumMET_pars = {builder_SumMET_ET, builder_SumMET_phi};
            vBuilder_SumMHT_pars = {builder_SumMHT_ET, builder_SumMHT_phi};

            builder_Jet          = std::make_shared<arrow::StructBuilder>(Jet_type, pool, std::move(vBuilder_Jet_pars));
            builder_EGamma       = std::make_shared<arrow::StructBuilder>(EGamma_type, pool, std::move(vBuilder_EGamma_pars));
            builder_Tau          = std::make_shared<arrow::StructBuilder>(Tau_type, pool, std::move(vBuilder_Tau_pars));
            builder_SumET        = std::make_shared<arrow::StructBuilder>(SumET_type, pool, std::move(vBuilder_SumET_pars));
            builder_SumHT        = std::make_shared<arrow::StructBuilder>(SumHT_type, pool, std::move(vBuilder_SumHT_pars));
            builder_SumMET       = std::make_shared<arrow::StructBuilder>(SumMET_type, pool, std::move(vBuilder_SumMET_pars));
            builder_SumMHT       = std::make_shared<arrow::StructBuilder>(SumMHT_type, pool, std::move(vBuilder_SumMHT_pars));

            listBuilder_Jet     = std::make_shared<arrow::ListBuilder>(pool, builder_Jet);
            listBuilder_EGamma  = std::make_shared<arrow::ListBuilder>(pool, builder_EGamma);
            listBuilder_Tau     = std::make_shared<arrow::ListBuilder>(pool, builder_Tau);
        }

        // append bx data (and initialize lists)
        inline void BxAppend(uint32_t orbit,
                             uint32_t bx) {

            builder_orbit->UnsafeAppend(orbit);
            builder_bx->UnsafeAppend(bx);

            PARQUET_THROW_NOT_OK(listBuilder_Jet->Append());
            PARQUET_THROW_NOT_OK(listBuilder_EGamma->Append());
            PARQUET_THROW_NOT_OK(listBuilder_Tau->Append());
        }

        // append number of objects data
        inline void NObjAppend(uint32_t nJet,
                               uint32_t nEGamma,
                               uint32_t nTau) {
            builder_nJet->UnsafeAppend(nJet);
            builder_nEGamma->UnsafeAppend(nEGamma);
            builder_nTau->UnsafeAppend(nTau);
        }

        inline void JetAppend(float fET,
                              float feta,
                              float fphi,
                              int32_t iso) {
            PARQUET_THROW_NOT_OK(builder_Jet->Append());

            builder_Jet_ET->UnsafeAppend(fET);
            builder_Jet_eta->UnsafeAppend(feta);
            builder_Jet_phi->UnsafeAppend(fphi);
            builder_Jet_iso->UnsafeAppend(iso);
        }

        inline void EGammaAppend(float fET,
                                 float feta,
                                 float fphi,
                                 int32_t iso) {
            PARQUET_THROW_NOT_OK(builder_EGamma->Append());

            builder_EGamma_ET->UnsafeAppend(fET);
            builder_EGamma_eta->UnsafeAppend(feta);
            builder_EGamma_phi->UnsafeAppend(fphi);
            builder_EGamma_iso->UnsafeAppend(iso);
        }

        inline void TauAppend(float fET,
                              float feta,
                              float fphi,
                              int32_t iso) {
            PARQUET_THROW_NOT_OK(builder_Tau->Append());

            builder_Tau_ET->UnsafeAppend(fET);
            builder_Tau_eta->UnsafeAppend(feta);
            builder_Tau_phi->UnsafeAppend(fphi);
            builder_Tau_iso->UnsafeAppend(iso);
        }

        inline void SumAppend(float fETEt,
                              float fHTEt,
                              float fETmissEt,
                              float fHTmissEt,
                              float fETmissPhi,
                              float fHTmissPhi) {
            PARQUET_THROW_NOT_OK(builder_SumET->Append());
            PARQUET_THROW_NOT_OK(builder_SumHT->Append());
            PARQUET_THROW_NOT_OK(builder_SumMET->Append());
            PARQUET_THROW_NOT_OK(builder_SumMHT->Append());

            builder_SumET_ET->UnsafeAppend(fETEt);
            builder_SumHT_ET->UnsafeAppend(fHTEt);
            builder_SumMET_ET->UnsafeAppend(fETmissEt);
            builder_SumMHT_ET->UnsafeAppend(fHTmissEt);
            builder_SumMET_phi->UnsafeAppend(fETmissPhi);
            builder_SumMHT_phi->UnsafeAppend(fHTmissPhi);
        }

        void Finish() {
            PARQUET_THROW_NOT_OK(builder_orbit->Finish(&arrayData[0]));
            PARQUET_THROW_NOT_OK(builder_bx->Finish(&arrayData[1]));

            PARQUET_THROW_NOT_OK(builder_nJet->Finish(&arrayData[2]));
            PARQUET_THROW_NOT_OK(listBuilder_Jet->Finish(&arrayData[3]));

            PARQUET_THROW_NOT_OK(builder_nEGamma->Finish(&arrayData[4]));
            PARQUET_THROW_NOT_OK(listBuilder_EGamma->Finish(&arrayData[5]));

            PARQUET_THROW_NOT_OK(builder_nTau->Finish(&arrayData[6]));
            PARQUET_THROW_NOT_OK(listBuilder_Tau->Finish(&arrayData[7]));

            PARQUET_THROW_NOT_OK(builder_SumET->Finish(&arrayData[8]));
            PARQUET_THROW_NOT_OK(builder_SumHT->Finish(&arrayData[9]));
            PARQUET_THROW_NOT_OK(builder_SumMET->Finish(&arrayData[10]));
            PARQUET_THROW_NOT_OK(builder_SumMHT->Finish(&arrayData[11]));
        };
    };
}



namespace ugt {
    struct arrow_data_block {
        // block n columns and column names
        static const unsigned int ncols = 515;
        static const std::vector<std::string> columns;

        // arrow builders
        arrow::UInt32Builder builderOrbitsSeen;
        arrow::UInt32Builder builderOrbitsDropped;
        arrow::UInt16Builder builderBx;
        std::vector<arrow::BooleanBuilder> vBuilderAlgos{512};

        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        inline void Append(uint32_t orbits_seen, uint32_t orbits_dropped, uint16_t bx,
                           std::vector<bool> algobits) {
            PARQUET_THROW_NOT_OK(builderOrbitsSeen.Append(orbits_seen));
            PARQUET_THROW_NOT_OK(builderOrbitsDropped.Append(orbits_dropped));
            PARQUET_THROW_NOT_OK(builderBx.Append(bx));
            for (int32_t i = 0; i < algobits.size(); i++) {
                PARQUET_THROW_NOT_OK(vBuilderAlgos[i].Append(algobits[i]));
            }
        }

        void Finish() {
            PARQUET_THROW_NOT_OK(builderOrbitsSeen.Finish(&arrayData[0]));
            PARQUET_THROW_NOT_OK(builderOrbitsDropped.Finish(&arrayData[1]));
            PARQUET_THROW_NOT_OK(builderBx.Finish(&arrayData[2]));
            for (uint32_t i = 0; i < vBuilderAlgos.size(); i++) {
                PARQUET_THROW_NOT_OK(vBuilderAlgos[i].Finish(&arrayData[i+3]));
            }
        }
    };
}



namespace bmtf {
    struct arrow_data_block {
        // block n columns and column names
        static const unsigned int ncols = 13;
        static const std::vector<std::string> columns;

        // arrow builders
        arrow::UInt32Builder builderOrbit;
        arrow::UInt32Builder builderOrbitsDropped;
        arrow::UInt16Builder builderBx;

        arrow::Int16Builder  builderValid;
        arrow::Int16Builder  builderPhi;
        arrow::Int16Builder  builderPhiB;
        arrow::Int16Builder  builderQual;
        arrow::Int16Builder  builderEta;
        arrow::Int16Builder  builderQEta;
        arrow::Int16Builder  builderStation;
        arrow::Int16Builder  builderWheel;
        arrow::Int16Builder  builderReserved;
        arrow::Int16Builder  builderLID;

        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        inline void Append(uint32_t orbit, uint32_t orbits_dropped, uint16_t bx,
                           int16_t valid, int16_t phi, int16_t phiB,
                           int16_t qual, int16_t eta, int16_t qeta,
                           int16_t station, int16_t wheel, int16_t reserved,
                           int16_t lid) {
            PARQUET_THROW_NOT_OK(builderOrbit.Append(orbit));
            PARQUET_THROW_NOT_OK(builderOrbitsDropped.Append(orbits_dropped));
            PARQUET_THROW_NOT_OK(builderBx.Append(bx));

            PARQUET_THROW_NOT_OK(builderValid.Append(valid));
            PARQUET_THROW_NOT_OK(builderPhi.Append(phi));
            PARQUET_THROW_NOT_OK(builderPhiB.Append(phiB));
            PARQUET_THROW_NOT_OK(builderQual.Append(qual));
            PARQUET_THROW_NOT_OK(builderEta.Append(eta));
            PARQUET_THROW_NOT_OK(builderQEta.Append(qeta));
            PARQUET_THROW_NOT_OK(builderStation.Append(station));
            PARQUET_THROW_NOT_OK(builderWheel.Append(wheel));
            PARQUET_THROW_NOT_OK(builderReserved.Append(reserved));
            PARQUET_THROW_NOT_OK(builderLID.Append(lid));
        }

        void Finish() {
            PARQUET_THROW_NOT_OK(builderOrbit.Finish(&arrayData[0]));
            PARQUET_THROW_NOT_OK(builderOrbitsDropped.Finish(&arrayData[1]));
            PARQUET_THROW_NOT_OK(builderBx.Finish(&arrayData[2]));
            PARQUET_THROW_NOT_OK(builderValid.Finish(&arrayData[3]));
            PARQUET_THROW_NOT_OK(builderPhi.Finish(&arrayData[4]));
            PARQUET_THROW_NOT_OK(builderPhiB.Finish(&arrayData[5]));
            PARQUET_THROW_NOT_OK(builderQual.Finish(&arrayData[6]));
            PARQUET_THROW_NOT_OK(builderEta.Finish(&arrayData[7]));
            PARQUET_THROW_NOT_OK(builderQEta.Finish(&arrayData[8]));
            PARQUET_THROW_NOT_OK(builderStation.Finish(&arrayData[9]));
            PARQUET_THROW_NOT_OK(builderWheel.Finish(&arrayData[10]));
            PARQUET_THROW_NOT_OK(builderReserved.Finish(&arrayData[11]));
            PARQUET_THROW_NOT_OK(builderLID.Finish(&arrayData[12]));
        }
    };
}

// const std::vector<std::string> ugmt::arrow_data_block::columns  = {"orbit", "bx", "interm", "pt", "ptunconstrained", "charge", "iso", "index", "qual", "phi", "phie", "eta", "etae", "dxy"};
// const std::vector<std::string> demux::arrow_data_block::columns = {"orbit", "bx", "ET", "type", "eta", "phi", "iso", "ETEt", "HTEt", "ETmissEt", "HTmissEt", "ETmissPhi", "HTmissPhi"};
// const std::vector<std::string> bmtf::arrow_data_block::columns = {"orbit", "orbits_dropped", "bx", "valid", "phi", "phiB", "qual", "eta", "qeta", "station", "wheel", "reserved"};



namespace multisrc {
    struct arrow_data_block_nested {
        // block n columns and column names
        static const unsigned int ncols = 14;
        static const std::vector<std::string> columns;

        // arrow memory pool
        arrow::MemoryPool* pool;

        // -- arrow builders
        std::shared_ptr<arrow::UInt32Builder> builder_orbit;
        std::shared_ptr<arrow::UInt16Builder> builder_bx;
        // muons
        std::shared_ptr<arrow::Int8Builder>   builder_nMuon;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_interm;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_pt;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_ptuncon;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_charge;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_iso;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_index;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_qual;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_phi;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_phie;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_Muon_etae;
        std::shared_ptr<arrow::Int8Builder>   builder_Muon_dxy;
        std::shared_ptr<arrow::StructBuilder> builder_Muon;
        // jets
        std::shared_ptr<arrow::Int8Builder>   builder_nJet;
        std::shared_ptr<arrow::FloatBuilder>  builder_Jet_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_Jet_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_Jet_phi;
        std::shared_ptr<arrow::Int8Builder>   builder_Jet_iso;
        std::shared_ptr<arrow::StructBuilder> builder_Jet;
        // egammas
        std::shared_ptr<arrow::Int8Builder>   builder_nEGamma;
        std::shared_ptr<arrow::FloatBuilder>  builder_EGamma_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_EGamma_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_EGamma_phi;
        std::shared_ptr<arrow::Int8Builder>   builder_EGamma_iso;
        std::shared_ptr<arrow::StructBuilder> builder_EGamma;
        // taus
        std::shared_ptr<arrow::Int8Builder>   builder_nTau;
        std::shared_ptr<arrow::FloatBuilder>  builder_Tau_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_Tau_eta;
        std::shared_ptr<arrow::FloatBuilder>  builder_Tau_phi;
        std::shared_ptr<arrow::Int8Builder>   builder_Tau_iso;
        std::shared_ptr<arrow::StructBuilder> builder_Tau;
        // sums
        std::shared_ptr<arrow::FloatBuilder>  builder_SumET_ET;
        std::shared_ptr<arrow::StructBuilder> builder_SumET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumHT_ET;
        std::shared_ptr<arrow::StructBuilder> builder_SumHT;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMET_ET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMHT_ET;
        std::shared_ptr<arrow::StructBuilder> builder_SumMET;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMET_phi;
        std::shared_ptr<arrow::FloatBuilder>  builder_SumMHT_phi;
        std::shared_ptr<arrow::StructBuilder> builder_SumMHT;

        // -- arrow struct parameters
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_Muon_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_Jet_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_EGamma_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_Tau_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumET_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumHT_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumMET_pars;
        std::vector<std::shared_ptr<arrow::ArrayBuilder> > vBuilder_SumMHT_pars;

        // -- arrow list builders
        std::shared_ptr<arrow::ListBuilder>   listBuilder_Muon;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_Jet;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_EGamma;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_Tau;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_SumET;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_SumHT;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_SumMET;
        std::shared_ptr<arrow::ListBuilder>   listBuilder_SumMHT;

        // arrow arrays
        std::vector<std::shared_ptr<arrow::Array> > arrayData{ncols};

        // struct constructor
        arrow_data_block_nested() {
            pool = arrow::default_memory_pool();

            std::shared_ptr<arrow::DataType> muon_type = arrow::struct_({
                arrow::field("interm",  arrow::int8()),
                arrow::field("pt",      arrow::float32()),
                arrow::field("ptuncon", arrow::float32()),
                arrow::field("charge",  arrow::int8()),
                arrow::field("iso",     arrow::int8()),
                arrow::field("index",   arrow::int8()),
                arrow::field("qual",    arrow::int8()),
                arrow::field("phi",     arrow::float32()),
                arrow::field("phie",    arrow::float32()),
                arrow::field("eta",     arrow::float32()),
                arrow::field("etae",    arrow::float32()),
                arrow::field("dxy",     arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> Jet_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("eta",    arrow::float32()),
                arrow::field("phi",    arrow::float32()),
                arrow::field("iso",    arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> EGamma_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("eta",    arrow::float32()),
                arrow::field("phi",    arrow::float32()),
                arrow::field("iso",    arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> Tau_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("eta",    arrow::float32()),
                arrow::field("phi",    arrow::float32()),
                arrow::field("iso",    arrow::int8()),
            });

            std::shared_ptr<arrow::DataType> SumET_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
            });

            std::shared_ptr<arrow::DataType> SumHT_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
            });

            std::shared_ptr<arrow::DataType> SumMET_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("phi",    arrow::float32()),
            });

            std::shared_ptr<arrow::DataType> SumMHT_type = arrow::struct_({
                arrow::field("ET",     arrow::float32()),
                arrow::field("phi",    arrow::float32()),
            });

            // --
            builder_orbit = std::make_shared<arrow::UInt32Builder>(pool);
            builder_bx    = std::make_shared<arrow::UInt16Builder>(pool);
            // muons
            builder_nMuon        = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_interm  = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_pt      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_ptuncon = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_charge  = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_iso     = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_index   = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_qual    = std::make_shared<arrow::Int8Builder>(pool);
            builder_Muon_phi     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_phie    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_eta     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_etae    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Muon_dxy     = std::make_shared<arrow::Int8Builder>(pool);
            // jets
            builder_nJet         = std::make_shared<arrow::Int8Builder>(pool);
            builder_Jet_ET       = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Jet_eta      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Jet_phi      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Jet_iso      = std::make_shared<arrow::Int8Builder>(pool);
            // egammas
            builder_nEGamma      = std::make_shared<arrow::Int8Builder>(pool);
            builder_EGamma_ET    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_EGamma_eta   = std::make_shared<arrow::FloatBuilder>(pool);
            builder_EGamma_phi   = std::make_shared<arrow::FloatBuilder>(pool);
            builder_EGamma_iso   = std::make_shared<arrow::Int8Builder>(pool);
            // taus
            builder_nTau         = std::make_shared<arrow::Int8Builder>(pool);
            builder_Tau_ET       = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Tau_eta      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Tau_phi      = std::make_shared<arrow::FloatBuilder>(pool);
            builder_Tau_iso      = std::make_shared<arrow::Int8Builder>(pool);
            // sums
            builder_SumET_ET     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumHT_ET     = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMET_ET    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMHT_ET    = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMET_phi   = std::make_shared<arrow::FloatBuilder>(pool);
            builder_SumMHT_phi   = std::make_shared<arrow::FloatBuilder>(pool);

            // --
            vBuilder_Muon_pars   = {builder_Muon_interm, builder_Muon_pt,
                                    builder_Muon_ptuncon, builder_Muon_charge,
                                    builder_Muon_iso, builder_Muon_index,
                                    builder_Muon_qual, builder_Muon_phi,
                                    builder_Muon_phie, builder_Muon_eta,
                                    builder_Muon_etae, builder_Muon_dxy};
            vBuilder_Jet_pars    = {builder_Jet_ET, builder_Jet_eta, builder_Jet_phi, builder_Jet_iso};
            vBuilder_EGamma_pars = {builder_EGamma_ET, builder_EGamma_eta, builder_EGamma_phi, builder_EGamma_iso};
            vBuilder_Tau_pars    = {builder_Tau_ET, builder_Tau_eta, builder_Tau_phi, builder_Tau_iso};
            vBuilder_SumET_pars  = {builder_SumET_ET};
            vBuilder_SumHT_pars  = {builder_SumHT_ET};
            vBuilder_SumMET_pars = {builder_SumMET_ET, builder_SumMET_phi};
            vBuilder_SumMHT_pars = {builder_SumMHT_ET, builder_SumMHT_phi};

            // --
            builder_Muon   = std::make_shared<arrow::StructBuilder>(muon_type,   pool, std::move(vBuilder_Muon_pars));
            builder_Jet    = std::make_shared<arrow::StructBuilder>(Jet_type,    pool, std::move(vBuilder_Jet_pars));
            builder_EGamma = std::make_shared<arrow::StructBuilder>(EGamma_type, pool, std::move(vBuilder_EGamma_pars));
            builder_Tau    = std::make_shared<arrow::StructBuilder>(Tau_type,    pool, std::move(vBuilder_Tau_pars));
            builder_SumET  = std::make_shared<arrow::StructBuilder>(SumET_type,  pool, std::move(vBuilder_SumET_pars));
            builder_SumHT  = std::make_shared<arrow::StructBuilder>(SumHT_type,  pool, std::move(vBuilder_SumHT_pars));
            builder_SumMET = std::make_shared<arrow::StructBuilder>(SumMET_type, pool, std::move(vBuilder_SumMET_pars));
            builder_SumMHT = std::make_shared<arrow::StructBuilder>(SumMHT_type, pool, std::move(vBuilder_SumMHT_pars));

            // --
            listBuilder_Muon   = std::make_shared<arrow::ListBuilder>(pool, builder_Muon);
            listBuilder_Jet    = std::make_shared<arrow::ListBuilder>(pool, builder_Jet);
            listBuilder_EGamma = std::make_shared<arrow::ListBuilder>(pool, builder_EGamma);
            listBuilder_Tau    = std::make_shared<arrow::ListBuilder>(pool, builder_Tau);
            listBuilder_SumET  = std::make_shared<arrow::ListBuilder>(pool, builder_SumET);
            listBuilder_SumHT  = std::make_shared<arrow::ListBuilder>(pool, builder_SumHT);
            listBuilder_SumMET = std::make_shared<arrow::ListBuilder>(pool, builder_SumMET);
            listBuilder_SumMHT = std::make_shared<arrow::ListBuilder>(pool, builder_SumMHT);
        }

        // append orbit and bx data (and initialize lists)
        inline void BxAppend(uint32_t orbit,
                             uint32_t bx) {
            PARQUET_THROW_NOT_OK(builder_orbit->Append(orbit));
            PARQUET_THROW_NOT_OK(builder_bx->Append(bx));

            PARQUET_THROW_NOT_OK(listBuilder_Muon->Append());
            PARQUET_THROW_NOT_OK(listBuilder_Jet->Append());
            PARQUET_THROW_NOT_OK(listBuilder_EGamma->Append());
            PARQUET_THROW_NOT_OK(listBuilder_Tau->Append());
            PARQUET_THROW_NOT_OK(listBuilder_SumET->Append());
            PARQUET_THROW_NOT_OK(listBuilder_SumHT->Append());
            PARQUET_THROW_NOT_OK(listBuilder_SumMET->Append());
            PARQUET_THROW_NOT_OK(listBuilder_SumMHT->Append());
        }

        // append object counts
        inline void NObjAppend(uint32_t nMuon,
                               uint32_t nJet,
                               uint32_t nEGamma,
                               uint32_t nTau) {

            PARQUET_THROW_NOT_OK(builder_nMuon->Append(nMuon));
            PARQUET_THROW_NOT_OK(builder_nJet->Append(nJet));
            PARQUET_THROW_NOT_OK(builder_nEGamma->Append(nEGamma));
            PARQUET_THROW_NOT_OK(builder_nTau->Append(nTau));
        };

        // append muon object to list
        inline void MuonAppend(uint32_t interm,
                               float fpt, float fptuncon,
                               int32_t chrg,
                               uint32_t iso, uint32_t index, uint32_t qual,
                               float fphi, float fphiext, float feta, float fetaext,
                               int32_t idxy) {
            PARQUET_THROW_NOT_OK(builder_Muon->Append());

            PARQUET_THROW_NOT_OK(builder_Muon_interm->Append(interm));
            PARQUET_THROW_NOT_OK(builder_Muon_pt->Append(fpt));
            PARQUET_THROW_NOT_OK(builder_Muon_ptuncon->Append(fptuncon));
            PARQUET_THROW_NOT_OK(builder_Muon_charge->Append(chrg));
            PARQUET_THROW_NOT_OK(builder_Muon_iso->Append(iso));
            PARQUET_THROW_NOT_OK(builder_Muon_index->Append(index));
            PARQUET_THROW_NOT_OK(builder_Muon_qual->Append(qual));
            PARQUET_THROW_NOT_OK(builder_Muon_phi->Append(fphi));
            PARQUET_THROW_NOT_OK(builder_Muon_phie->Append(fphiext));
            PARQUET_THROW_NOT_OK(builder_Muon_eta->Append(feta));
            PARQUET_THROW_NOT_OK(builder_Muon_etae->Append(fetaext));
            PARQUET_THROW_NOT_OK(builder_Muon_dxy->Append(idxy));
        };

        // append jet object to list
        inline void JetAppend(float fET,
                              float feta,
                              float fphi,
                              int32_t iso) {
            PARQUET_THROW_NOT_OK(builder_Jet->Append());

            PARQUET_THROW_NOT_OK(builder_Jet_ET->Append(fET));
            PARQUET_THROW_NOT_OK(builder_Jet_eta->Append(feta));
            PARQUET_THROW_NOT_OK(builder_Jet_phi->Append(fphi));
            PARQUET_THROW_NOT_OK(builder_Jet_iso->Append(iso));
        }

        inline void EGammaAppend(float fET,
                                 float feta,
                                 float fphi,
                                 int32_t iso) {
            PARQUET_THROW_NOT_OK(builder_EGamma->Append());

            PARQUET_THROW_NOT_OK(builder_EGamma_ET->Append(fET));
            PARQUET_THROW_NOT_OK(builder_EGamma_eta->Append(feta));
            PARQUET_THROW_NOT_OK(builder_EGamma_phi->Append(fphi));
            PARQUET_THROW_NOT_OK(builder_EGamma_iso->Append(iso));
        }

        inline void TauAppend(float fET,
                              float feta,
                              float fphi,
                              int32_t iso) {
            PARQUET_THROW_NOT_OK(builder_Tau->Append());

            PARQUET_THROW_NOT_OK(builder_Tau_ET->Append(fET));
            PARQUET_THROW_NOT_OK(builder_Tau_eta->Append(feta));
            PARQUET_THROW_NOT_OK(builder_Tau_phi->Append(fphi));
            PARQUET_THROW_NOT_OK(builder_Tau_iso->Append(iso));
        }

        inline void SumAppend(float fETEt,
                              float fHTEt,
                              float fETmissEt,
                              float fHTmissEt,
                              float fETmissPhi,
                              float fHTmissPhi) {
            PARQUET_THROW_NOT_OK(builder_SumET->Append());
            PARQUET_THROW_NOT_OK(builder_SumHT->Append());
            PARQUET_THROW_NOT_OK(builder_SumMET->Append());
            PARQUET_THROW_NOT_OK(builder_SumMHT->Append());

            PARQUET_THROW_NOT_OK(builder_SumET_ET->Append(fETEt));
            PARQUET_THROW_NOT_OK(builder_SumHT_ET->Append(fHTEt));
            PARQUET_THROW_NOT_OK(builder_SumMET_ET->Append(fETmissEt));
            PARQUET_THROW_NOT_OK(builder_SumMHT_ET->Append(fHTmissEt));
            PARQUET_THROW_NOT_OK(builder_SumMET_phi->Append(fETmissPhi));
            PARQUET_THROW_NOT_OK(builder_SumMHT_phi->Append(fHTmissPhi));
        }

        // close builders and arrays
        void Finish() {
            PARQUET_THROW_NOT_OK(builder_orbit     ->Finish(&arrayData[ 0]));
            PARQUET_THROW_NOT_OK(builder_bx        ->Finish(&arrayData[ 1]));
            // GMT data
            PARQUET_THROW_NOT_OK(builder_nMuon     ->Finish(&arrayData[ 2]));
            PARQUET_THROW_NOT_OK(listBuilder_Muon  ->Finish(&arrayData[ 3]));
            // CALO data
            PARQUET_THROW_NOT_OK(builder_nJet      ->Finish(&arrayData[ 4]));
            PARQUET_THROW_NOT_OK(listBuilder_Jet   ->Finish(&arrayData[ 5]));

            PARQUET_THROW_NOT_OK(builder_nEGamma   ->Finish(&arrayData[ 6]));
            PARQUET_THROW_NOT_OK(listBuilder_EGamma->Finish(&arrayData[ 7]));

            PARQUET_THROW_NOT_OK(builder_nTau      ->Finish(&arrayData[ 8]));
            PARQUET_THROW_NOT_OK(listBuilder_Tau   ->Finish(&arrayData[ 9]));

            PARQUET_THROW_NOT_OK(listBuilder_SumET ->Finish(&arrayData[10]));
            PARQUET_THROW_NOT_OK(listBuilder_SumHT ->Finish(&arrayData[11]));
            PARQUET_THROW_NOT_OK(listBuilder_SumMET->Finish(&arrayData[12]));
            PARQUET_THROW_NOT_OK(listBuilder_SumMHT->Finish(&arrayData[13]));
        };
    };
}

#endif