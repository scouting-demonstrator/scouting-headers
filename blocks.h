#ifndef BLOCKS_H
#define BLOCKS_H

#include <cstdint>
#include <vector>
#include <string>
#include "scales.h"



namespace ugmt {
    // struct ugmt_hw_data_block{
    struct hw_data_block{
        std::vector<int> vorbit;
        std::vector<int> vbx;
        std::vector<int> vinterm;
        std::vector<int> vipt;
        std::vector<int> viptunconstrained;
        std::vector<int> vcharge;
        std::vector<int> viso;
        std::vector<int> vindex;
        std::vector<int> vqual;
        std::vector<int> viphi;
        std::vector<int> viphiext;
        std::vector<int> vieta;
        std::vector<int> vietaext;
        std::vector<int> vidxy;

        unsigned int size() {return vipt.size();}
        bool empty() {return vipt.empty();}
        static const unsigned int ncols = 14;
        static const std::vector<std::string> columns;

        std::vector<int> iAt(int i) {
            return {
                vorbit[i],
                vbx[i],
                vinterm[i],
                vipt[i],
                viptunconstrained[i],
                vcharge[i],
                viso[i],
                vindex[i],
                vqual[i],
                viphi[i],
                viphiext[i],
                vieta[i],
                vietaext[i],
                vidxy[i],
            };
        }

        std::vector<float> fAt(int i){
            float eta = vieta[i]*ugmt::scales::eta_scale;
            float etaext = vietaext[i]*ugmt::scales::eta_scale;
            float phi = viphi[i]*ugmt::scales::phi_scale;
            float phiext = viphiext[i]*ugmt::scales::phi_scale;

            if (phiext>M_PI) {
                phiext = phiext - 2*M_PI;
            }
            if (phi>M_PI) {
                phi = phi - 2*M_PI;
            }

            float pt               = (vipt[i]-1)*ugmt::scales::pt_scale;
            float ptunconstrained  = (viptunconstrained[i]-1)*ugmt::scales::ptunconstrained_scale;

            return {
                float(vorbit[i]),
                float(vbx[i]),
                float(vinterm[i]),
                pt,
                ptunconstrained,
                float(vcharge[i]),
                float(viso[i]),
                float(vindex[i]),
                float(vqual[i]),
                phi,
                phiext,
                eta,
                etaext,
                float(vidxy[i]),
            };
        }
    };

    struct muon {
        uint32_t f;
        uint32_t s;
        uint32_t extra;
    };

    struct block {
        uint32_t bx;
        uint32_t orbit;
        muon mu[16];
    };
}



namespace demux {
    // struct calo_objects_hw_data_block{
    struct hw_data_block {
        std::vector<int> vorbit;
        std::vector<int> vbx;
        std::vector<int> vET;
        std::vector<int> vType;
        std::vector<int> vEta;
        std::vector<int> vPhi;
        std::vector<int> vIso;

        unsigned int size() {return vET.size();}
        bool empty() {return vET.empty();}
        static const unsigned int ncols = 13;
        static const std::vector<std::string> columns;

        std::vector<int> iAt(int i) {
            int eta_s = vEta[i];

            if (eta_s > 127) {
                eta_s = eta_s - 256;
            }

            return {
                vorbit[i],
                vbx[i],
                vET[i],
                vType[i],
                eta_s,
                vPhi[i],
                vIso[i]
            };
        }

        std::vector<float> fAt(int i) {
            int eta_s = vEta[i];

            if (eta_s > 127){
                eta_s = eta_s - 256;
            }

            float phi = vPhi[i]*demux::scales::phi_scale;
            float eta = eta_s*demux::scales::eta_scale;
            float et  = vET[i]*demux::scales::et_scale;

            if (phi>2*M_PI) {
                phi = phi - 2.*M_PI;
            }

            return {
                float(vorbit[i]),
                float(vbx[i]),
                et,
                float(vType[i]),
                eta,
                phi,
                float(vIso[i])
            };
        }
    };

    // struct block_calo_packed{
    struct block {
        uint32_t header;
        uint32_t bx;
        uint32_t orbit;
        uint32_t frame[56];     // +8 for extra word containing link number
    };

    struct block_v2 {
        uint32_t header;
        uint32_t bx;
        uint32_t orbit;
        uint32_t link0;
        uint32_t jet1[6];
        uint32_t link1;
        uint32_t jet2[6];
        uint32_t link2;
        uint32_t egamma1[6];
        uint32_t link3;
        uint32_t egamma2[6];
        uint32_t link4;
        uint32_t empty[6];
        uint32_t link5;
        uint32_t sum[6];
        uint32_t link6;
        uint32_t tau1[6];
        uint32_t link7;
        uint32_t tau2[6];
    };
}



namespace ugt {
    struct block {
        uint32_t frame[48];
    };

    struct header {
        uint32_t frame[8];
    };

    struct trailer {
        uint32_t frame[144];
    };
}



namespace bmtf {
    struct block_8x {
        uint32_t frame[48];
    };

    struct block_16x {
        uint32_t frame[96];
    };

    struct stub_block_8x {
        uint64_t stub[32];
    };

    struct stub_block_16x {
        uint64_t stub[64];
    };

    struct orbit_header_8x {
        uint32_t frame[8];
    };

    struct orbit_header_16x {
        uint32_t frame[16];
    };

    struct orbit_trailer_8x {
        uint32_t frame[136];
    };

    struct orbit_trailer_16x {
        uint32_t frame[160];
    };

    struct packet_trailer_8x {
        uint32_t frame[8];
    };

    struct packet_trailer_16x {
        uint32_t frame[16];
    };
}



const std::vector<std::string> ugmt::hw_data_block::columns  = {"orbit", "bx", "interm", "pt", "ptunconstrained", "charge", "iso", "index", "qual", "phi", "phie", "eta", "etae", "dxy"};
const std::vector<std::string> demux::hw_data_block::columns = {"orbit", "bx", "ET", "type", "eta", "phi", "iso", "ETEt", "HTEt", "ETmissEt", "HTmissEt", "ETmissPhi", "HTmissPhi"};

#endif